@php
$notifOrder = 0;
$notifProduction = 0;
$notifPurchase = 0;
$notifDelivery = 0;
$user = \Auth::user();
@endphp

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('index') }}"><img src="{{ asset('logo.png') }}" class="logo-l" style="height: 50px;padding:2px"></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('index') }}"><img src="{{ asset('icon.png') }}" class="logo-m" style="width: 46px"></a>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ request()->is('/') ? 'active' : '' }}"><a class="nav-link" href="{{ route('index') }}"><i class="fas fa-th-large"></i> <span>Home</span></a></li>

            @if ($user->isAdministrator())
            <li class="{{ request()->is('jabatans*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('jabatans.index') }}"><i class="fas fa-briefcase"></i> <span>Jabatan</span></a></li>
            @endif

            @if ($user->isAdministrator() || $user->isKeuangan())
            <li class="{{ request()->is('users*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('users.index') }}"><i class="fas fa-users"></i> <span>Pegawai</span></a></li>
            @endif

            @if ($user->isAdministrator() || $user->isAdmin())
            <li class="{{ request()->is('products*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('products.index') }}"><i class="fas fa-box"></i> <span>Produk</span></a></li>
            @endif

            <li class="{{ request()->is('orders*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('orders.index') }}"><i class="fas fa-shopping-cart"></i> <span>Pesanan</span></a></li>
        </ul>
    </aside>
</div>
