@extends('layouts.app')
@section('css')
<style type="text/css">
    .card-recent {
      height: 250px;
    }
</style>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Koperasi Karyawan</h1>
            </div>

            <div class="section-body">

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                      <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                          <i class="far fa-user"></i>
                        </div>
                        <div class="card-wrap">
                          <div class="card-header">
                            <h4>Limit Bulanan</h4>
                          </div>
                          <div class="card-body">
                            Rp. {{ number_format(request()->user()->limit_balance, 0, ',', '.') }}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                      <div class="card card-statistic-1">
                        <div class="card-icon bg-danger">
                          <i class="far fa-newspaper"></i>
                        </div>
                        <div class="card-wrap">
                          <div class="card-header">
                            <h4>Sisa Limit</h4>
                          </div>
                          <div class="card-body">
                            Rp. {{ number_format(request()->user()->limit_remaining_user, 0, ',', '.') }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                <div class="row">
                    <div class="col-md-12">
                        <div style="margin-top: 5vh">
                            <center>
                                <img src="{{ asset('logo.png') }}" style="width:300px;margin-bottom:35px">
                                <hr>
                                <p style="font-size: 14pt">
                                    PT. Dhanarmas Concern adalah salah satu perusahaan tekstil yang berdiri sejak tahun 1964 di Bandung.
                                    Orientasi produk kami yang sebagian besar adalah ekspor, menjadikan kami terus mengembangkan diri baik dari kualitas produk yang dihasilkan
                                    maupun sumber daya manusia yang menjadi motor penggerak.
                                </p>
                                <p>Jl. Moh. Toha Jl. Cisirung No.KM 6.8, Pasawahan, Kec. Dayeuhkolot, Kabupaten Bandung, Jawa Barat 40256</p>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
