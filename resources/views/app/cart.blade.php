@extends('layouts.app')
@section('css')
<style type="text/css">
    .card-recent {
      height: 250px;
    }
</style>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <div class="d-flex justify-content-between items-center w-full" style="width: 100%">
                    <h1>Keranjang Belanja</h1>
                    <h1>
                        Sisa Limit: Rp. {{ number_format(request()->user()->limit_remaining_user, 0, ',', '.') }}
                    </h1>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered table-hover shopping-cart-wrap">
                                    <thead class="text-muted">
                                    <tr>
                                      <th scope="col">Produk</th>
                                      <th scope="col" width="150">Jumlah</th>
                                      <th scope="col" width="180">Total Harga</th>
                                      <th scope="col" width="200" class="text-right">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $total = 0;
                                        @endphp
                                        @foreach($carts as $cart)
                                        <form method="POST" action="{{ route('app.cart.update-cart', $cart->id) }}">
                                            @csrf
                                          <tr>
                                            <td>
                                              <figure class="media mt-2">
                                                <div class="img-wrap"><img src="{{ $cart->product->photo_url }}" style="max-width: 100px; padding-right:1rem"></div>
                                                <figcaption class="media-body">
                                                  <h6 class="title text-truncate">{{ $cart->product->name }}</h6>
                                                  <div class="param param-inline small">
                                                    <dt>Harga : </dt>
                                                    <dd>{{ $cart->product->price_rupiah }}</dd>
                                                  </div>
                                                </figcaption>
                                              </figure>
                                            </td>
                                            <td>
                                              <input type="number" value="{{ $cart->quantity }}" name="quantity" class="form-control">
                                            </td>
                                            <td>
                                              <div class="price-wrap" align="right">
                                                <var class="price">{{ $cart->getSubTotal($cart->product->price) }}</var>
                                              </div>
                                            </td>
                                            <td class="text-right">
                                              <button type="submit" class="btn btn-outline-warning">Update</button>
                                              <a href="{{ route('app.cart.remove-from-cart', $cart->id) }}" class="btn btn-outline-danger">Hapus</a>
                                            </td>
                                          </tr>
                                        </form>
                                        @php
                                        $total += $cart->quantity * $cart->product->price;
                                        @endphp
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th class="text-right" colspan="3">Total</th>
                                        <td class="text-right" colspan="2">
                                            <div class="price-wrap">
                                            <span class="price"><b>Rp. {{ number_format($total, 0, ',', '.') }}</b></span>
                                            </div>
                                        </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                @if(count($carts) > 0)
                                <div class="d-flex justify-content-end">
                                    <form method="POST" action="{{ route('app.orders.store') }}">
                                        @csrf
                                        <input type="hidden" value="{{ $total }}" name="total">
                                        <button class="btn btn-primary">
                                            <i class="fa fa-chevron-right"></i> Ajukan Pesanan
                                        </button>
                                    </form>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
