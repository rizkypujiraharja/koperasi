@extends('layouts.app')
@section('css')
<style type="text/css">
    .card-recent {
      height: 250px;
    }
</style>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar Pesanan</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">

                                  <div class="table-responsive">
                                    <table class="table table-striped">
                                      <tr>
                                        <th>No</th>
                                        <th>Kode Pesanan</th>
                                        <th>Total Harga</th>
                                        <th>Status</th>
                                        <th>Tanggal Pengajuan</th>
                                        <th>#</th>
                                      </tr>
                                      @forelse ($orders as $item)
                                      <tr>
                                          <td>{{ (($orders->currentPage()-1) * $orders->perPage()) + $loop->iteration }}</td>
                                          <td>{{ $item->invoice_code }}</td>
                                          <td>{{ $item->total_harga }}</td>
                                          <td align="center">{!! $item->label_status !!}</td>
                                          <td>{{ $item->tanggal_pesan }}</td>
                                          <td>
                                              <a href="{{ route('app.orders.show', $item) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Lihat Pesanan"> <span class="fa fa-eye"></span></a>
                                          </td>
                                      </tr>
                                      @empty
                                      <tr>
                                          <td colspan="7" align="center">
                                              <div class="empty-state" data-height="400" style="height: 400px;">
                                                  <div class="empty-state-icon">
                                                    <i class="fas fa-question"></i>
                                                  </div>
                                                  <h2>Data Tidak Ditemukan</h2>
                                                  <p class="lead">
                                                    Maaf kami tidak dapat menemukan data.
                                                  </p>
                                              </div>
                                          </td>
                                      </tr>
                                      @endforelse
                                    </table>
                                  </div>
                                  <div class="float-right">
                                    {{ $orders->links() }}
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
