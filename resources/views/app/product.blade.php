@extends('layouts.app')
@section('css')
<style type="text/css">
    .card-recent {
      height: 250px;
    }
</style>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar Produk</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    @foreach ($products as $index => $product)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <article class="article article-style-b">
                           <div class="article-header">
                              <div class="article-image" data-background="{{ $product->photo_url }}">
                              </div>
                           </div>
                           <div class="article-details">
                              <div class="article-title text-black">
                                <a href="#"  onclick="showDetail({{ $index }})">
                                    <h6>{{ $product->name }}</h6>
                                </a>
                              </div>
                              <div class="d-flex justify-content-between">
                                <strong class="text-info">{{ $product->price_rupiah }}</strong> <br>
                                <span>Stock ({{ $product->stock }})</span>
                              </div>
                              <div class="mt-2">
                                  <button class="btn btn-sm btn-outline-info" onclick="showDetail({{ $index }})">
                                      Pesan <i class="fas fa-chevron-right"></i>
                                  </button>
                              </div>
                           </div>
                        </article>
                     </div>
                    @endforeach
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modalDetail">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form action="{{ route('app.product.add-to-cart') }}" method="POST">
                @csrf
                <input type="hidden" name="product_id" id="product-id">
                <div class="modal-header">
                    <h5 class="modal-title" id="product-name"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="" alt="" srcset="" id="product-image" class="img-fluid">
                    <h6 class="text-info" id="product-price"></h6>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Tambahkan ke keranjang</button>
                </div>
            </form>
          </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var products = @json($products)

        function showDetail(index) {
            const product = products.data[index]

            $('#modalDetail').modal('show')
            $('#product-name').html(product.name)
            $('#product-image').attr('src', product.photo_url)
            $('#product-price').html(product.price_rupiah)
            $('#product-id').val(product.id)
        }
    </script>
@endsection
