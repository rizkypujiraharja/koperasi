<table border="1">
<tr>
        <td colspan="4" align="center">{{ $filename }}</td>
    </tr>
    <tr></tr>
    <tr>
        <td align="center">NO</td>
        <td align="center">NAMA PRODUK</td>
        <td align="center">HARGA</td>
        <td align="center">STOCK</td>
    </tr>
    @foreach ($products as $product)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->stock }}</td>
        </tr>
    @endforeach
</table>
