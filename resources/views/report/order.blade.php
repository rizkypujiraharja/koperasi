<table>
    <tr>
        <td colspan="6" align="center">{{ $filename }}</td>
    </tr>
    <tr></tr>
    <tr>
        <td align="center">NO</td>
        <td align="center">KODE PESANAN</td>
        <td align="center">NAMA PESANAN</td>
        <td align="center">TOTAL HARGA</td>
        <td align="center">STATUS</td>
        <td align="center">TANGGAL PEMESANAN</td>
    </tr>
    @foreach ($orders as $order)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $order->invoice_code }}</td>
            <td>{{ $order->employee_name }}</td>
            <td>{{ $order->total_harga }}</td>
            <td>{{ $order->text_status }}</td>
            <td>{{ $order->tanggal_pesan }}</td>
        </tr>
    @endforeach
</table>
