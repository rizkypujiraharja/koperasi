<!DOCTYPE html>
<html>
<head>
  <title>Report Order</title>
  <style type="text/css">
    .table {
        width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
        border-collapse: collapse;
    }

    .table-bordered th, .table-bordered td{
      border: 1px solid #dee2e6;
    }

    .table th, .table td{
      padding: 3px;
    }

    .align-center th, .align-center td{
      text-align: center;;
    }

    .align-left th, .align-left td{
      text-align: left;
    }

    h3, h4{
      color: #60bc9a;
    }

  </style>
</head>
<body>
  <img src="{{ public_path('logo.png') }}" height="50px">
  <h3>Invoice : {{$order->invoice_code }}</h3>
  <table class="table align-left">
    <tr>
        <th width="30%">Nama Pegawai</th>
        <td width="70%">{{ $order->employee_name }}</td>
    </tr>
    <tr>
        <th>Total Pesanan</th>
        <td>{{ $order->total_harga }}</td>
    </tr>
  </table>

  <table class="table table-bordered align-center">
    <tr>
        <th>No</th>
        <th>Nama Produk</th>
        <th>Harga</th>
        <th>Qty</th>
        <th>Sub Total</th>
      </tr>
      @php $no = 1 @endphp
      @foreach ($order->details as $detail)
          <tr >
              <td>{{ $no }}</td>
              <td>{{ $detail->product_name }}</td>
              <td>{{ $detail->harga}}</td>
              <td>{{ $detail->quantity}}</td>
              <td>{{ $detail->sub_total_harga}}</td>
          </tr>
          @php $no++ @endphp
      @endforeach
  </table>
</body>
</html>
