<?php

use Illuminate\Support\Facades\Route;

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'ProfileController@edit')->name('profile');
    Route::put('/profile', 'ProfileController@update')->name('profile.update');
});

Route::group(
    ['middleware' => ['auth', 'role:administrator|admin|keuangan'], 'namespace' => 'Admin'],
    function () {
        Route::get('/', 'HomeController@index')->name('index');

        Route::resource('users', 'UserController');
        Route::resource('jabatans', 'JabatanController');

        Route::get('products/report', 'ProductController@report')->name('products.report');
        Route::resource('products', 'ProductController');

        Route::get('orders/report', 'OrderController@report')->name('orders.report');
        Route::get('/orders/export/{order}', 'OrderController@export')->name('orders.export');
        Route::resource('orders', 'OrderController')->only(['show', 'index']);
        Route::patch('/orders/{order}/change-status', 'OrderController@changeStatus')->name('orders.change-status');
    }
);

Route::group(
    ['middleware' => ['auth', 'role:user'], 'prefix' => 'app', 'as' => 'app.'],
    function () {
        Route::get('/', 'HomeController@index')->name('index');
        Route::get('product', 'ProductController@index')->name('product');
        Route::post('product/add-to-cart', 'ProductController@addToCart')->name('product.add-to-cart');
        Route::get('cart', 'ProductController@cart')->name('cart');
        Route::get('cart/remove-from-cart/{cart_id}', 'ProductController@removeFromCart')->name('cart.remove-from-cart');
        Route::post('cart/update-cart/{cart_id}', 'ProductController@updateCart')->name('cart.update-cart');

        Route::resource('orders', 'OrderController')->only(['show', 'index', 'store']);
        Route::get('/orders/export/{order}', 'OrderController@export')->name('orders.export');
    }
);
