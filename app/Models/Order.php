<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['status'];
    protected $appends = ['label_status', 'tanggal_pesan'];

    public function generateCode()
    {
        $monthyear = date('ymd');
        $currentCode = 'INV/' . $monthyear . '/KPR/DMC/';

        $last = self::whereYear('created_at', date('Y'))->latest()->first();

        if (is_null($last)) {
            $code = $currentCode . '00001';
        } else {
            $lastNum = (int) substr($last->invoice_code, -5);
            $code = $currentCode .  str_pad($lastNum + 1, 5, '0', STR_PAD_LEFT);
        }

        return $code;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function getLabelStatusAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<span class="badge badge-warning">Menunggu persetujuan</span>';
            case 1:
                return '<span class="badge badge-info">Menyiapkan pesanan</span>';
            case 2:
                return '<span class="badge badge-light">Pesanan siap diambil</span>';
            case 3:
                return '<span class="badge badge-success">Selesai</span>';
            case 4:
                return '<span class="badge badge-dark">Dibatalkan</span>';
            case 5:
                return '<span class="badge badge-danger">Ditolak</span>';
        }
    }

    public function getTextStatusAttribute()
    {
        switch ($this->status) {
            case 0:
                return 'Menunggu persetujuan';
            case 1:
                return 'Menyiapkan pesanan';
            case 2:
                return 'Pesanan siap diambil';
            case 3:
                return 'Selesai';
            case 4:
                return 'Dibatalkan';
            case 5:
                return 'Ditolak';
        }
    }

    public function getTanggalPesanAttribute()
    {
        return $this->created_at->isoFormat('dddd, D MMMM Y HH:mm');
    }

    public function getTotalHargaAttribute()
    {
        return "Rp. " . number_format($this->details->sum('sub_total_price'), 0, ',', '.');
    }

    public function isWaiting()
    {
        return $this->status == 0;
    }

    public function isProcessing()
    {
        return $this->status == 1;
    }

    public function isReady()
    {
        return $this->status == 2;
    }

    public function isFinished()
    {
        return $this->status == 3;
    }

    public function isCanceled()
    {
        return $this->status == 4;
    }

    public function isRejected()
    {
        return $this->status == 5;
    }
}
