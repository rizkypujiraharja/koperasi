<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'product_id', 'product_name', 'quantity', 'price', 'sub_total_price', 'order_id'
    ];

    protected $guarderd = [];

    public function getHargaAttribute()
    {
        return "Rp. " . number_format($this->price, 0, ',', '.');
    }

    public function getSubTotalHargaAttribute()
    {
        return "Rp. " . number_format($this->sub_total_price, 0, ',', '.');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
