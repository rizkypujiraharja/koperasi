<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Cart extends Model
{
    use HasFactory;

    /**
     * Get the product that owns the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function scopeMine()
    {
        return $this->where('user_id', auth()->id());
    }

    public function getSubtotal($price)
    {
        $subTotal = $this->quantity * $price;
        return 'Rp. ' . number_format($subTotal, 0, ',', '.');
    }
}
