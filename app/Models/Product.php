<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class Product extends Model
{
    use SoftDeletes;

    protected $appends = ['photo_url', 'price_rupiah'];

    public function generateCode()
    {
        $this->code = "B" . str_pad($this->id, 5, '0', STR_PAD_LEFT);
        $this->save();
    }

    public function getPriceRupiahAttribute()
    {
        return 'Rp. ' . number_format($this->price, 0, ',', '.');
    }

    public function getPhotoUrlAttribute()
    {
        if ($this->photo) {
            return Storage::url($this->photo);
        }
        return asset('product.png');
    }
}
