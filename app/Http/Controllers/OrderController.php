<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::where('user_id', $request->user()->id)
            ->with('details')
            ->latest()
            ->paginate(config('app.per_page'));

        return view('app.orders', compact('orders'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $carts = Cart::where('user_id', $user->id)->with('product')->get();

        if ($request->total > $user->limit_remaining_user) {
            return redirect()->back()->with('alert-error', 'Jumlah pesanan melebihi sisa limit. Sisa Limit: Rp.' . number_format(request()->user()->limit_remaining_user, 0, ',', '.'));
        }

        try {
            DB::beginTransaction();

            $order = new Order;
            $order->user_id = $user->id;
            $order->invoice_code = $order->generateCode();
            $order->employee_name = $user->name;
            $order->employee_phone = $user->phone;
            $order->status = 0;
            $order->save();

            foreach ($carts as $cart) {
                $product = $cart->product;

                $detail = new OrderDetail;
                $detail->order_id = $order->id;
                $detail->product_id = $product->id;
                $detail->product_name = $product->name;
                $detail->price = $product->price;
                $detail->quantity = $cart->quantity;
                $detail->sub_total_price = $detail->price * $detail->quantity;
                $detail->save();

                $cart->delete();
            }

            DB::commit();
            return redirect()->route('app.orders.index')->with('alert-success', 'Berhasil membuat pesanan');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('alert-error', 'Terjadi kesalahan, Silahkan ulangi!');
        }
    }

    public function show(Order $order)
    {
        $user = Auth::user();
        if ($order->user_id != $user->id) {
            abort(403);
        }

        return view('app.show-order', compact('order'));
    }

    public function export(Request $request, Order $order)
    {
        $user = Auth::user();
        if ($order->user_id != $user->id) {
            abort(403);
        }

        $code = str_replace('/', '-', $order->invoice_code);

        $filename = 'Invoice ' . $code;

        $pdf = PDF::loadView('report.invoice', compact('order'));
        return $pdf->download($filename . '.pdf');
    }
}
