<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\{Order, Product, Production, Customer, ProductVariant};
use App\Exports\{OrderReport, OrderDetailReport};
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Excel;
use PDF;

use App\Mail\InvoiceOrder;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::latest()->with('details');

        if ($request->has('search')) {
            $query = '%' . $request->search . '%';
            $orders->where('employee_name', 'LIKE', $query);
        }

        if ($request->daterange) {
            $daterange = explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else {
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");

            $date_from = Carbon::createFromFormat('Y-m-d', $from);
            $date_to = Carbon::createFromFormat('Y-m-d', $to);
        }

        $orders->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to);

        $orders = $orders->paginate(config('app.per_page'))->withQueryString();

        return view('admin.order.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $order->with('details');
        $products = $order->details->groupBy('product_name');

        return view('admin.order.show', compact('order', 'products'));
    }

    public function toggleStatus(Order $order)
    {
        try {
            \DB::beginTransaction();

            $order->status = 1;
            $order->save();

            $productions = $order->details->map(function ($detail) {
                $detail->sub_total_size = $detail->quantity * $detail->product_size;
                return $detail->only('product_name', 'product_variant_id', 'quantity', 'product_size', 'sub_total_size');
            });
            $totalSize = $productions->sum('sub_total_size');

            $production = new Production;
            $production->code = $production->generateCode();
            $production->type = 'order';
            $production->order_id = $order->id;
            $production->status = 0;
            $production->total_size = $totalSize;
            $production->save();
            $production->details()->createMany($productions->toArray());

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->back()->with('alert-success', 'Berhasil Menambah Data Produksi');
    }

    public function report(Request $request)
    {
        $orders = Order::query();

        if ($request->daterange) {
            $daterange = explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else {
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");

            $date_from = Carbon::createFromFormat('Y-m-d', $from);
            $date_to = Carbon::createFromFormat('Y-m-d', $to);
        }

        if ($request->has('search')) {
            $query = '%' . $request->search . '%';
            $orders->where('employee_name', 'LIKE', $query);
        }

        $orders = $orders->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->get();

        $filename = 'Laporan Pesanan ' . $date_from->isoFormat('D MMMM Y') . ' - ' . $date_to->isoFormat('D MMMM Y');
        return Excel::download(new OrderReport($orders, $filename), $filename . '.xlsx');
    }

    public function changeStatus(Request $request, Order $order)
    {
        $order->status = $request->status;
        $order->load('details');
        if ($request->status == 3 || $request->status == 4) {
            foreach ($order->details as $detail) {
                $product = $detail->product;
                if ($request->status == 3) {
                    $product->stock -= $detail->quantity;
                } elseif ($request->status == 4) {
                    $product->stock += $detail->quantity;
                }
                $product->save();
            }
        }

        $order->save();

        return redirect()->back()->with('alert-success', 'Berhasil mengubah status pesanan');
    }

    public function export(Request $request, Order $order)
    {
        $code = str_replace('/', '-', $order->invoice_code);

        $filename = 'Invoice ' . $code;

        $pdf = PDF::loadView('report.invoice', compact('order'));
        return $pdf->download($filename . '.pdf');
    }
}
