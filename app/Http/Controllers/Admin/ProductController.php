<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Exports\StockProductReport;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Excel;
use Image;
use Storage;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::latest();

        if ($request->has('search')) {
            $query = '%' . $request->search . '%';
            $products->where('name', 'LIKE', $query);
        }

        $products = $products->paginate(config('app.per_page'))->withQueryString();

        return view('admin.product.index', compact('products'));
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:5',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
        ], [], [
            'name' => 'nama produk',
            'price' => 'harga produk'
        ]);

        try {
            \DB::beginTransaction();
            $product = new Product;
            $product->name = $request->name;
            $product->price = $request->price;
            $product->stock = $request->stock;
            if (!is_null($request->photo)) {
                $file = $request->file('photo');
                $path = $file->hashName('photos');

                $image = Image::make($file);
                $image->fit(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::put($path, (string) $image->encode());
                $product->photo = $path;
            }

            $product->save();
            $product->generateCode();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->route('products.index')->with('alert-success', 'Berhasil Menambah Produk');
    }

    public function show(Product $product)
    {
        return view('admin.product.show', compact('product'));
    }

    public function edit(Product $product)
    {
        return view('admin.product.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'name' => 'required|string|min:5',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
        ], [], [
            'name' => 'nama produk',
            'price' => 'harga produk'
        ]);

        try {
            \DB::beginTransaction();
            $product->name = $request->name;
            $product->price = $request->price;
            $product->stock = $request->stock;
            if (!is_null($request->photo)) {
                $file = $request->file('photo');
                $path = $file->hashName('photos');

                $image = Image::make($file);
                $image->fit(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::put($path, (string) $image->encode());
                $product->photo = $path;
            }

            $product->save();
            $product->generateCode();

            \DB::commit();
        } catch (\Exception $e) {
            dd($e);
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->route('products.show', $product)->with('alert-success', 'Berhasil Mengubah Produk');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')->with('alert-success', 'Berhasil menghapus produk');
    }

    public function report(Request $request)
    {
        $date = Carbon::now()->isoFormat('D MMMM Y');

        $products = Product::get();

        $filename = 'Laporan Stock Produk' . $date;

        return Excel::download(new StockProductReport($products, $filename), $filename . '.xlsx');
    }
}
