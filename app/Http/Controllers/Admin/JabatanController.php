<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Jabatan;
use Illuminate\Http\Request;

class JabatanController extends Controller
{

    public function index(Request $request)
    {
        $jabatans = Jabatan::latest();

        if ($request->has('search')) {
            $query = '%' . $request->search . '%';
            $jabatans->where('name', 'LIKE', $query);
        }

        $jabatans = $jabatans->paginate(config('app.per_page'))->withQueryString();

        return view('admin.jabatans.index', compact('jabatans'));
    }

    public function create()
    {
        return view('admin.jabatans.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:50|string',
        ], [], [
            'name' => 'nama',
        ]);

        $jabatan = new Jabatan;
        $jabatan->name = $request->name;
        $jabatan->save();

        return redirect()->route('jabatans.index')->with('alert-success', 'Berhasil menambah jabatan!');
    }

    public function show(Jabatan $jabatan)
    {
        return view('admin.jabatans.show', compact('jabatan'));
    }

    public function edit(Jabatan $jabatan)
    {
        return view('admin.jabatans.edit', compact('jabatan'));
    }

    public function update(Request $request, Jabatan $jabatan)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:50|string',
        ], [], [
            'name' => 'nama',
        ]);

        $jabatan->name = $request->name;
        $jabatan->save();

        return redirect()->route('jabatans.index')->with('alert-success', 'Berhasil mengubah jabatan!');
    }

    public function destroy(Jabatan $jabatan)
    {
        $jabatan->delete();

        return redirect()->route('jabatans.index')->with('alert-success', 'Berhasil menghapus data jabatan');
    }
}
