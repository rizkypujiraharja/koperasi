<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::paginate(12);

        return view('app.product', compact('products'));
    }

    public function addToCart(Request $request)
    {
        $product = Product::find($request->product_id);
        $user_id = Auth::id();

        $cart_product = Cart::where('user_id', $user_id)
            ->where('product_id', $product->id)
            ->first();

        if (is_null($cart_product)) {
            $cart_product = new Cart;
            $cart_product->user_id = $user_id;
            $cart_product->product_id = $product->id;
            $cart_product->quantity = 1;
            $cart_product->save();
        } else {
            $cart_product->increment('quantity');
        }

        return redirect()->back()->with('alert-success', 'Produk berhasil ditambahkan ke keranjang!');
    }

    public function removeFromCart(Request $request, $cart_id)
    {
        $user_id = Auth::id();

        $cart_product = Cart::where('user_id', $user_id)
            ->where('id', $cart_id)
            ->firstOrFail();

        $cart_product->delete();

        return redirect()->back()->with('alert-success', 'Produk berhasil dihapus dari keranjang!');
    }

    public function updateCart(Request $request, $cart_id)
    {
        $user_id = Auth::id();

        $cart_product = Cart::where('user_id', $user_id)
            ->where('id', $cart_id)
            ->firstOrFail();

        $cart_product->quantity = $request->quantity;
        $cart_product->save();

        return redirect()->back()->with('alert-success', 'Jumlah produk berhasil diperbarui!');
    }

    public function cart()
    {
        $user_id = Auth::id();

        $carts = Cart::where('user_id', $user_id)
            ->get();

        return view('app.cart', compact('carts'));
    }
}
