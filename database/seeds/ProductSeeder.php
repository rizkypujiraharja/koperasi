<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ["Minyak Goreng Bimoli 1L", 17000, 50],
            ["Minyak Goreng Bimoli 2L", 35000, 30],
            ["Gula Putih Gulaku, 1kg", 17000, 75],
            ["Beras 5kg", 60000, 50],
            ["Teh Sariwangi", 7500, 1000],
            ["Mie Indomie Rebus Ayam Bawang", 2500, 200],
            ["Mie Indomie Goreng", 3000, 200],
            ["Telur Ayam 1/2kg", 11000, 30],
            ["Telur Ayam 1kg", 22000, 20],
            ["Kecap Bango Botol 135ml", 10000, 40],
            ["Kecap Bango 550ml", 55000, 40],
            ["Saus Sambal ABC Botol 135ml", 8000, 50],
            ["Sarden 135ml", 9000, 75],
            ["Susu Kental Manis Bendera Kaleng", 15000, 40],
            ["Kopi Hitam Kapal Api 165gr", 8000, 50],
        ];

        foreach ($products as $product) {
            $p = new Product;
            $p->name = $product[0];
            $p->price = $product[1];
            $p->stock = $product[2];
            $p->save();
            $p->generateCode();
            sleep(1);
        }
    }
}
