<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Order, OrderDetail, Product};
use App\User;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::latest()->first();
        for ($i = 0; $i < 5; $i++) {
            $order = new Order;
            $order->user_id = $user->id;
            $order->employee_name = $user->name;
            $order->employee_phone = $user->phone;
            $order->invoice_code = $order->generateCode();
            $order->status = rand(0, 4);
            $order->save();

            $products = Product::inRandomOrder()->limit(rand(2, 5))->get();
            foreach ($products as $product) {
                $detail = new OrderDetail;
                $detail->order_id = $order->id;
                $detail->product_id = $product->id;
                $detail->product_name = $product->name;
                $detail->price = $product->price;
                $detail->quantity = rand(1, 5);
                $detail->sub_total_price = $detail->price * $detail->quantity;
                $detail->save();
            }
            sleep(1);
        }
    }
}
