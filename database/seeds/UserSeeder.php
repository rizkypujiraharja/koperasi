<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->nip = '111111111111';
        $user->name = 'Bambang Nurhidayat';
        $user->address = 'Baleendah, Bandung';
        $user->phone = '0892783648234';
        $user->photo = '';
        $user->email = 'bambang@dhanarmas.com';
        $user->password = bcrypt('secret123');
        $user->role = 'administrator';
        $user->save();
    }
}
