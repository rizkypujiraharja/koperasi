<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nip', 100);
            $table->string('name');
            $table->string('address')->nullable();
            $table->string('phone', 100)->nullable();
            $table->string('photo')->nullable();
            $table->string('email')->unique();
            $table->double('limit_balance', 10, 2)->default(1000000);
            $table->string('password');
            $table->string('role');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
